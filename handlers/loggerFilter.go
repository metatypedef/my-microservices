package handlers

import (
	"log"
	"net/http"
	"web-server/data"
)

type LoggerFilter struct {
	logger *log.Logger
}

func (filter* LoggerFilter) handleReq(req *http.Request, data []byte) {
	filter.logger.Printf("\r\n")
	filter.logger.Printf("%s %s %s\n", req.Method, req.URL, req.Proto)

	for k, v := range req.Header {
		filter.logger.Printf("Header[%q] = %q\n", k, v)
	}

	filter.logger.Printf("Host = %q\n", req.Host)
	filter.logger.Printf("RemoteAddr = %q\n", req.RemoteAddr)

	if data != nil {
		filter.logger.Printf("Body = %s\n", data)
	}

	filter.logger.Printf("\r\n\n")
}

func (filter* LoggerFilter) handleError(req *http.Request, err error) {
	filter.logger.Printf("\r\nHonda handled request with URL.Path = %q\n Error:%q\r\n\n",
		req.URL.Path, err.Error())
}

func (filter* LoggerFilter) handleResp(obj data.Serializable) {
	filter.logger.Print("Response: ")
	obj.ToJSON(filter.logger.Writer())
	filter.logger.Printf("\r\n\n")
}
