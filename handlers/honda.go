package handlers

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type Honda struct {
	loggerFilter *LoggerFilter
}

func NewHonda(l *log.Logger) *Honda {
	return &Honda{loggerFilter: &LoggerFilter{l}}
}

func (h* Honda) ServeHTTP(writer http.ResponseWriter, req *http.Request) {
	// we want body data
	data, error := ioutil.ReadAll(req.Body)

	// check for errors
	if error != nil || len(data) == 0 {
		// we need to log errors
		h.loggerFilter.handleError(req, error)
		http.Error(writer, "Ooops!!", http.StatusBadRequest)
		return
	}

	// if has no errors then everything is well
	h.loggerFilter.handleReq(req, data)
	fmt.Fprintf(writer, "URL.Path = %q\n Body:%q", req.URL.Path, data)
}