package handlers

import (
	"log"
	"net/http"
	"web-server/data"
)

type Product struct {
	loggerFilter *LoggerFilter
}

func NewProduct(l *log.Logger) *Product {
	return &Product{loggerFilter: &LoggerFilter{l}}
}

func (p* Product) ServeHTTP(writer http.ResponseWriter, req *http.Request) {
	if req.Method == http.MethodGet {
		p.getProducts(writer, req)
		return
	}

	// all other methods not allowed
	writer.WriteHeader(http.StatusMethodNotAllowed)
}

func (p* Product) getProducts(writer http.ResponseWriter, req *http.Request) {
	// if has no errors then everything is well
	p.loggerFilter.handleReq(req, nil)

	// set content type
	writer.Header().Add("Content-Type", "application/json")

	products := data.GetProducts()
	error := products.ToJSON(writer)
	if error != nil {
		http.Error(writer, "Unable to encode to JSON", http.StatusInternalServerError)
	}

	p.loggerFilter.handleResp(&products)
}