package data

import (
	"io"
)

type Serializable interface {
	ToJSON(w io.Writer) error
}