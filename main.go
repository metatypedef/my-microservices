package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
	"web-server/handlers"
	"github.com/nicholasjackson/env"
)

var bindAddres = env.String("BIND_ADDRESS", false, ":7777", "Bind address for http server")

func main() {
	env.Parse()

	logger := log.New(os.Stdout, "product-api", log.LstdFlags)

	honda := handlers.NewHonda(logger)
	acura := handlers.NewAcura(logger)
	product := handlers.NewProduct(logger)

	serveMux := http.NewServeMux()
	serveMux.Handle("/honda", honda)
	serveMux.Handle("/acura", acura)
	serveMux.Handle("/product", product)

	server := &http.Server{
		Addr:         *bindAddres,
		Handler:      serveMux,
		IdleTimeout:  120 * time.Second,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	go func() {
		err := server.ListenAndServe()
		if err != nil {
			log.Fatal(err)
		}
	}()

	signalChan := make(chan os.Signal)
	signal.Notify(signalChan, os.Interrupt)
	signal.Notify(signalChan, os.Kill)

	sig := <-signalChan
	logger.Println("Received terminate and start gracefully shutdown", sig)

	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	server.Shutdown(ctx)
}
